# VenenuX minetest 0.4 mod basic_materials

This mod provides a small selection of "basic" materials 
and items that other mods should use when possible -- things 
like steel bars and chains, wire, plastic strips and sheets, and more.

The whole idea here is to eliminate duplication from mod to mod, 
and reduce recipe conflicts.

## About 

Most of the items in this mod came from Home Decor, Pipeworks, Technic, 
and Gloopblocks, some of which are intended as direct replacements for 
similar items, along with a few new bits and bobs that I hope modders will find useful.

Basically-speaking, if two or more mods provide some equivalent item, 
and it's something basic that other mods could make use of, and it's 
something that could have otherwise made a good addition to minetest_game 
such that everyone should have that item, then those duplicate items 
and their recipes should be removed from their respective mods, 
a single such item and recipe added to this mod instead, and 
those mods made to depend on this one.

All item names begin with basic_materials:, and all of them 
have aliases back to their original names, where applicable.

In order shown in the image above, they are:
brass_block, brass_ingot, cement_block, chain_brass, chain_steel, chainlink_brass, chainlink_steel, concrete_block
copper_strip, copper_wire, empty_spool, energy_crystal_simple, gear_steel, gold_wire, heating_element, ic
motor, oil_extract, padlock, paraffin, plastic_sheet, plastic_strip, silicon, silver_wire
steel_bar, steel_strip, steel_wire, terracotta_base, wet_cement

![screenshot.png](screenshot.png)

## Usage

for detailed usage information, please see [the Basic materials Thread](https://forum.minetest.net/viewtopic.php?t=21000) on the Minetest forum.

Our doucmentation is on [https://codeberg.org/venenux/venenux-minetest/src/branch/master/mods/basic_materials/basic_materials-en.md](https://codeberg.org/venenux/venenux-minetest/src/branch/master/mods/basic_materials/basic_materials-en.md)

## Download: 

* mientest 5.2 -> https://gitlab.com/VanessaE/basic_materials/-/archive/master/basic_materials-master.tar.ga
* mientest 0.4 -> https://codeberg.org/minenux/minetest-mod-basic_materials/archive/0.4.17.tar.gz

## Dependencies: 

Recommends: Moreores.

## License: 

LGPL 3.0 for code, CC-by-SA 4.0 for media and everything else. [LICENSE](LICENSE)
